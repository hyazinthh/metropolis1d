clear;
X = [4 3 2 1 -6 -10 18];

mean_ref = mean(X);
M2_ref = moment(X, 2);
M3_ref = moment(X, 3);
M4_ref = moment(X, 4);

n = 0;
mean = 0;
M2 = 0;
M3 = 0;
M4 = 0;

for i = 1:length(X)
    n1 = n;
    n = n + 1;
    
    d = X(i) - mean;
    dn = d / n;
    dn2 = dn * dn;
    t = d * dn * n1;
    
    mean = mean + dn;
    M4 = M4 + t * dn2 * (n * n - 3 * n + 3) + 6 * dn2 * M2 - 4 * dn * M3;
    M3 = M3 + t * dn * (n - 2) - 3 * dn * M2;
    M2 = M2 + t;
end

M2 = M2 / n;
M3 = M3 / n;
M4 = M4 / n;



