clear;

path = '../results/';
ds = 'set_features';

data = load([path ds]);
p = data(:, 16);

X = cell(1, length(p));

for i = 1:length(p)
    X{i} = num2str(p(i));
end

C = categorical(X);
countcats(C)

figure;
histogram(C);
xlabel('Optimal large step probability');
ylabel('Count');


