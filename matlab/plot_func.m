clear;

path = '../results/';
ds = 'foo';

data = load([path ds '/input']);

X = data(1:2:end);
Y = data(2:2:end);

figure;
plot(X, Y);
