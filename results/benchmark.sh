#!/bin/bash

cmd="../build/src/benchmark/benchmark"
samples=25600
runs=1024
fileFeatures="set_features"
fileSamples="set_samples"

for dir in */
do
	printf "Benchmarking ${dir}: "
	input=$(cat "${dir}input")

	echo "${input}" | ${cmd} "${samples}" "${runs}" "${fileFeatures}" "${fileSamples}"
done
