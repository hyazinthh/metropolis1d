#!/bin/bash

cmd="../build/src/generator/generator"

postfix=1
prefix="rnd"
n=$1

for i in `seq 1 ${n}`
do
	while [ -d "${prefix}${postfix}" ]; do
		postfix=$((postfix+1))
	done

	dir="${prefix}${postfix}"
	mkdir $dir

	${cmd} > "${dir}/input"
done
