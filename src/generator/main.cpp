#include <iostream>
#include <random>
#include <map>

#include "common.h"
#include "sum.h"

struct Peak {
    real min, max;
};

/**
 * @brief Generates a random number in the interval [0, 1)
 * @return a random number.
 */
real rnd() {
    static std::random_device rd;
    static std::mt19937 rng(rd());
    static std::uniform_real_distribution<real> uniform;
    return uniform(rng);
}

/**
 * @brief Generates a random function.
 * @param outSamples returns the samples of the generated function on success.
 * @return true on success, false otherwise.
 */
bool generate(std::map<real, real, CmpReal>& outSamples) {

    real d = 100 + rnd() * 900;
    real maxPeaks = 1;
    real maxHeight = 100 + rnd() * 900;
    real maxDev = 0.3 + rnd() * 0.2;
    real maxDevDelta = 0.3;
    int minSubSamples = 10;
    int maxSubSamples = 30;
    real contrib = rnd() * 0.2;
    real contribDev = rnd() * 0.1;
    real baseHeight = rnd() * 0.01 * maxHeight;

    // Generate peaks
    int numPeaks = static_cast<int>(std::round(1 + rnd() * (maxPeaks - 1)));

    std::map<real, Peak, CmpReal> peaks;
    for (int i = 0; i < numPeaks; i++) {
        real x;

        do {
            x = rnd() * d;
        } while(peaks.find(x) != peaks.end());

        Peak p = {x, x};
        peaks[x] = p;
    }

    // Peak extents
    for (auto it = peaks.begin(); it != peaks.end(); ++it) {

        auto n = std::next(it);

        real a = ((it != peaks.begin()) ? std::prev(it)->second.max : 0) + EPSILON;
        real b = ((n != peaks.end()) ? n->second.min : d) - EPSILON;

        if (a > b) {
            continue;
        }

        real c1 = std::max(real(0), std::min(real(1), contrib + contribDev * (-1 + 2 * rnd())));
        real c2 = std::max(real(0), std::min(real(1), contrib + contribDev * (-1 + 2 * rnd())));

        real x = it->first;
        real x1 = x - c1 * (x - a);
        real x2 = x + c2 * (b - x);

        it->second.min = x1;
        it->second.max = x2;
    }

    // Generate samples for base
    std::map<real, real, CmpReal> samples;

    int n = 20;
    real h = baseHeight;

    for (int i = 0; i <= n; ++i) {

        real t = static_cast<real>(i) / static_cast<real>(n);
        real x = t * d;

        bool inside = false;

        for (auto& p : peaks) {
            inside = inside || (x > p.second.min && x < p.second.max);
        }

        if (inside) {
            continue;
        }

        h += 0.2 * baseHeight * (-1 + 2 * rnd());
        h = std::max(0.0, std::min(2 * baseHeight, h));

        samples[x] = h;
    }


    // Generate peak samples
    for (auto it = peaks.begin(); it != peaks.end(); ++it) {
        real a = it->second.min;
        real m = it->first;
        real b = it->second.max;

        if (m - a < EPSILON || b - m < EPSILON) {
            continue;
        }

        real height = 10 * baseHeight + rnd() * maxHeight;
        real roughness = rnd();

        {
            real d = m - a;
            int sub = minSubSamples + static_cast<int>(std::round(roughness * rnd() * (maxSubSamples - minSubSamples)));
            int n = std::min(sub, static_cast<int>(std::floor(d / EPSILON)));
            real dev = 0;

            for (int i = 0; i < n; i++) {
                real t = static_cast<real>(i) / static_cast<real>(n);
                real x = a + t * d;

                dev += maxDevDelta * height * roughness * (-1 + 2 * rnd());
                dev = std::max(-maxDev * roughness * height, std::min(maxDev * roughness * height, dev));

                samples[x] = std::max(real(0), t * height + (1 - t) * baseHeight + dev);
            }
        }

        {
            real d = b - m;
            int sub = minSubSamples + static_cast<int>(std::round(roughness * rnd() * (maxSubSamples - minSubSamples)));
            int n = std::min(sub, static_cast<int>(std::floor(d / EPSILON)));
            real dev = 0;

            for (int i = 0; i < n; i++) {
                real t = static_cast<real>(i) / static_cast<real>(n);
                real x = m + t * d;

                dev += maxDevDelta * height * roughness * (-1 + 2 * rnd());
                dev = std::max(-maxDev * roughness * height, std::min(maxDev * roughness * height, dev));

                samples[x] = std::max(real(0), (1 - t) * height + t * baseHeight + dev);
            }
        }

        samples[a] = baseHeight;
        samples[b] = baseHeight;
    }

    // Check if there is at least one sample > 0
    bool nonZero = false;

    for (auto& s : samples) {
        if (s.second > 0) {
            nonZero = true;
            break;
        }
    }

    if (nonZero) {
        outSamples = std::move(samples);
        return true;
    } else {
        return false;
    }
}

int main() {

    // Repeat generation until we get a function that is non-zero
    bool success;
    std::map<real, real, CmpReal> samples;

    do {
        success = generate(samples);
    }
    while (!success);

    // Output
    for (auto it = samples.begin(); it != samples.end(); ++it) {
        std::cout <<  it->first << ", " << it->second;

        if (std::next(it) != samples.end()) {
            std::cout << ", ";
        }
    }

    std::cout << std::endl;

    // Finished
    return 0;
}
