#include <iostream>
#include <fstream>
#include <omp.h>
#include <chrono>
#include <unistd.h>

#include "common.h"
#include "sampled_function.h"
#include "metropolis.h"
#include "string.h"

/**
 * @brief Parses command line arguments.
 * @param argc number of arguments.
 * @param argv array of arguments.
 * @param samples returns the number of samples for a single run.
 * @param runs returns the number of runs to be used.
 * @param fileFeatures returns the path of the output file that saves features and the optimal pLarge value.
 * @param fileSamples returns the path of the output file that saves function samples and the optimal pLarge value.
 * @return true if parsing was successful, false otherwise.
 */
bool parseArgs(int argc, char* argv[], size_t& samples, size_t& runs, std::string& fileFeatures, std::string& fileSamples) {

    const std::string usage("Usage: metropolis1D samples runs featuresFile funcFile");

    // Read options
    int opt;

    while ((opt = getopt(argc, argv, "")) != -1) {
        switch (opt) {
            default:
                std::cout << usage << std::endl;
                return false;
        }
    }

    // Check number of arguments
    if (argc - optind != 4) {
        std::cout << "Error: Wrong number of arguments" << std::endl
                  << usage << std::endl;
        return false;
    }

    // Read samples
    if (!String::parse(argv[optind], samples)) {
        std::cout << "Error: Cannot parse '" << argv[optind] << "' as unsigned long" << std::endl;
        return false;
    }

    if (samples == 0) {
        std::cout << "Error: samples cannot be 0" << std::endl;
        return false;
    }

    // Read runs
    if (!String::parse(argv[optind + 1], runs)) {
        std::cout << "Error: Cannot parse '" << argv[optind + 1] << "' as unsigned long" << std::endl;
        return false;
    }

    if (runs == 0) {
        std::cout << "Error: runs cannot be 0" << std::endl;
        return false;
    }

    // Read file names
    fileFeatures = argv[optind + 2];
    fileSamples = argv[optind + 3];

    // Success
    return true;
}


/**
 * @brief Parses the input as function samples.
 * @param input comma separated list of (x,y) value pairs.
 * @param out returns the parsed function samples.
 * @return true if parsing was successful, false otherwise.
 */
bool parseInput(const std::string& input, SampledFunction::Samples& out) {

    // Tokenize input
    std::vector<std::string> tokens;
    String::tokenize(input, ',', tokens);

    if (tokens.size() % 2 != 0) {
        std::cout << "Error: Odd number of (x,y) values" << std::endl;
        return false;
    }

    // Parse strings
    SampledFunction::Samples samples;

    for (size_t i = 0; i < tokens.size(); i += 2) {

        real x, y;

        if (!String::parse(tokens[i], x)) {
            std::cout << "Error: Cannot parse '" << tokens[i] << "' as real" << std::endl;
            return false;
        }

        if (!String::parse(tokens[i + 1], y)) {
            std::cout << "Error: Cannot parse '" << tokens[i + 1] << "' as real" << std::endl;
            return false;
        }

        if (y < 0) {
            std::cout << "Error: Function values cannot be negative (" << y << " < 0)" << std::endl;
            return false;
        }

        samples[x] = y;
    }

    // Success
    out = std::move(samples);
    return true;
}

// Save features in a simple array. If more features are to be added, this has
// to be adjusted as well as the benchmark() function.
typedef std::array<real, 15> Features;

/**
 * @brief Benchmarks metropolis sampling for the given function, using the provided parameters.
 * @param f the function to be sampled.
 * @param samples the number of samples to be drawn in total.
 * @param runs determines how often the benchmark is to be repeated for each pLarge value.
 * @param features returns representative statistics about the function.
 * @return the optimal pLarge value for the function.
 */
real benchmark(IntegrableFunction& f, size_t samples, size_t runs, Features& features) {

    const int N = 11;

    Features avgFeatures = {};
    std::vector<real> avgError(N, 0);

    for (int i = 0; i < N; i++) {

        real pLarge = static_cast<real>(i) / (N - 1);
        std::vector<Metropolis::Stats> stats(runs);

        // Run tests in parallel
        size_t p = static_cast<size_t>(omp_get_max_threads());

        #pragma omp parallel num_threads(static_cast<int>(p))
        {
            // Get thread rank
            size_t r = static_cast<size_t>(omp_get_thread_num());

            // Repeat so that we get desired iterations in total over all threads
            size_t n = runs / p + ((r < runs % p) ? 1 : 0);

            // Initialize and run metropolis sampler n times
            Metropolis m(f);

            for (size_t j = 0; j < n; j++) {

                // Run metropolis
                m.initialize(128, 4096);
                m.run(samples, pLarge);

                // Compute index
                size_t idx = r * (runs / p) + std::min(r, runs % p) + j;

                // Save raw stats
                stats[idx] = m.getStats();
            }
        }

        // Average error over all runs
        real r = static_cast<real>(runs);

        for (auto& s : stats) {
            avgError[i] += s.ksError / r;
        }

        // We only need the features for a single arbritary
        // pLarge value, since they should be consistent across different
        // values anyway (except 0 and 1). Here we choose 0.5
        if (i == (N - 1) / 2) {
           for (auto& s : stats) {
                avgFeatures[0] += s.acceptedLargeSteps.getRatio() / r;
                avgFeatures[1] += s.acceptedSmallSteps.getRatio() / r;
                avgFeatures[2] += s.nonZeroLargeSteps.getRatio() / r;
                avgFeatures[3] += s.diffQuotient.getMin() / r;
                avgFeatures[4] += s.diffQuotient.getMax() / r;
                avgFeatures[5] += s.diffQuotient.getMean() / r;
                avgFeatures[6] += s.diffQuotient.getVariance() / r;
                avgFeatures[7] += s.diffQuotient.getSkewness() / r;
                avgFeatures[8] += s.diffQuotient.getKurtosis() / r;
                avgFeatures[9] += s.value.getMin() / r;
                avgFeatures[10] += s.value.getMax() / r;
                avgFeatures[11] += s.value.getMean() / r;
                avgFeatures[12] += s.value.getVariance() / r;
                avgFeatures[13] += s.value.getSkewness() / r;
                avgFeatures[14] += s.value.getKurtosis() / r;
            }
        }
    }

    // Find optimal pLarge value
    real pOpt = NaN;
    real error = MAX;

    for (int i = 0; i < N; i++) {
        if (avgError[i] < error) {
            error = avgError[i];
            pOpt = static_cast<real>(i) / (N - 1);
        }
    }

    // Return optimal value and features
    features = std::move(avgFeatures);
    return pOpt;
}

int main(int argc, char* argv[]) {

    // Parse command line
    size_t metroSamples, runs = 0;
    std::string fileFeatures, fileSamples;

    if (!parseArgs(argc, argv, metroSamples, runs, fileFeatures, fileSamples)) {
        return EXIT_FAILURE;
    }

    // Read input
    std::string input;
    std::getline(std::cin, input);

    // Parse input
    SampledFunction::Samples samples;

    if (!parseInput(input, samples)) {
        return EXIT_FAILURE;
    }

    if (samples.size() < 2) {
        std::cout << "Error: empty input" << std::endl;
        return EXIT_FAILURE;
    }

    SampledFunction f(samples);
    auto d = f.getDomain();

    // Open files
    std::ofstream ofFeatures(fileFeatures, std::ios::app);

    if (!ofFeatures.is_open()) {
        std::cout << "Error: could not open file '" << fileFeatures << "'" << std::endl;
        return EXIT_FAILURE;
    }

    std::ofstream ofSamples(fileSamples, std::ios::app);

    if (!ofSamples.is_open()) {
        std::cout << "Error: could not open file '" << fileSamples << "'" << std::endl;
        return EXIT_FAILURE;
    }

    // Benchmark
    typedef std::chrono::milliseconds ms;
    typedef std::chrono::duration<real> Duration;
    typedef std::chrono::high_resolution_clock Clock;

    auto start = Clock::now();

    Features features;
    real p = benchmark(f, metroSamples, runs, features);

    Duration dur = Clock::now() - start;
    std::cout << "Benchmarking complete, took "
              << std::chrono::duration_cast<ms>(dur).count()
              << " ms" << std::endl;

    // Output to features file
    for (real x : features) {
        ofFeatures << x << ", ";
    }

    ofFeatures << p << std::endl;

    // Output to samples file
    const int N = 1024;

    for (int i = 0; i < N; i++) {
        real u = static_cast<real>(i) / (N - 1);
        real x = d.min + u * (d.max - d.min);

        ofSamples << f(x) << ", ";
    }

    ofSamples << p << std::endl;

    // Done
    return EXIT_SUCCESS;
}
