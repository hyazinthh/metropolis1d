#ifndef FUNCTION_H
#define FUNCTION_H

#include "common.h"

/**
 * @brief Interface for real-valued, 1-dimensional functions.
 */
class Function {

    public:

        /**
         * @brief Domain of a function, i.e. its lower and upper bound.
         */
        struct Domain {
            inline Domain() = default;

            inline Domain(real a, real b)
                : min(a), max(b) {
            }

            real min, max;
        };

        virtual ~Function() = default;

        /**
         * @brief Returns the domain of the function.
         * @return the domain of the function.
         */
        virtual Domain getDomain() = 0;

        /**
         * @brief Evaluates the function at coordinate @p x.
         * @param x the x-coordinate at which the function is to be evaluated.
         * @return the value of the function at @p x.
         */
        virtual real operator()(real x) = 0;
};

#endif // FUNCTION_H
