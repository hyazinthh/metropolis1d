#ifndef METROPOLIS_H
#define METROPOLIS_H

#include <random>
#include <map>

#include "common.h"
#include "integrable_function.h"
#include "iterative_stats.h"
#include "counter.h"
#include "sum.h"

/**
 * @brief Class encapsulating the metroplis sampling algorithm.
 */
class Metropolis
{
    public:

        /**
         * @brief Map to record distribution of samples.
         */
        typedef std::map<real, Sum, CmpReal> SampleDistribution;

         /**
         * @brief Struct for a single sample.
         */
        struct Sample {
            real u = 0;         ///< The coordinate of the sample in primary sample space.
            real value = 0;     ///< The value of the sample, i.e. sample(u)
            Sum weight;         ///< The weight of the sample.
        };

        /**
         * @brief Struct for statistics regarding metropolis sampling.
         */
        struct Stats {
            real ksError = 0;              ///< Kolmogorov-Smirnov error statistic
            size_t sampleCount = 0;        ///< Current number of samples drawn, not including the current sample / seed.
            Counter acceptedLargeSteps;    ///< Acceptance ratio of large steps.
            Counter acceptedSmallSteps;    ///< Acceptance ratio of small steps.
            Counter nonZeroLargeSteps;     ///< Ratio of large steps with non zero contribution steps.
            IterativeStats diffQuotient;   ///< Statistics about the difference quotient for smalls steps, i.e. (f(b) - f(a)) / (b - a)
            IterativeStats value;          ///< Statistics about the function values (for large steps).
        };

        /**
         * @brief Constructs a new Metropolis sampler.
         * @param func the function to be sampled.
         */
        Metropolis(IntegrableFunction& func);

        /**
         * @brief Initializes the metropolis algorithm by regularly sampling the function.
         * @param samplesMin the minimum number of samples, ignored if @p samplesMin = 0.
         * @param samplesMax the maximum number of samples, ignored if @p samplesMax < @p samplesMin.
         * @return true if b > 0, false otherwise.
         *
         * Initializes the metropolis algorithm by regularly sampling the function, computing the seed.
         * Starting with @p samplesMin samples, the number is doubled if the f(x) = 0 for all samples x.
         * This is repeated until the count exceeds @p samplesMax or some contribution is recorded.
         */
        bool initialize(size_t samplesMin, size_t samplesMax = 0);

        /**
         * @brief Runs the metropolis sampling algorithm.
         * @param samples the number of samples to draw.
         * @param pLarge the large step probability, must be within [0, 1]
         *
         * The most recent invocation of Metropolis::initialize() must be successful, for this function
         * to have any effect. Metropolis::run(a, p); Metropolis::run(b, p) is equivalent to Metropolis::run(c, p),
         * if a + b = c and Metropolis::initialize is not called between the two invocations.
         */
        void run(size_t samples, real pLarge);

        /**
         * @brief Returns the current statistics.
         * @return reference to a Stats struct that is continuously updated.
         */
        const Stats& getStats() {
            return stats;
        }

    private:

        /**
         * @brief Mutates the given real value.
         * @param value the value to mutate.
         * @return the mutated value.
         */
        real mutate(real value);

        /**
         * @brief Transforms primary sample space to domain space.
         * @param u coordinate in primary sample space.
         * @return corresponding domain space coordinate.
         */
        inline real pss2ds(real u) {
            auto d = f.getDomain();
            return d.min + u * (d.max - d.min);
        }

        /**
         * @brief Samples the function, given a coordinate in primary sample space.
         * @param u the coordinate in primary sample space.
         * @return the value of the function.
         */
        inline real sample(real u) {
            return f(pss2ds(u));
        }

        /**
         * @brief Generates a random number in the interval [0, 1)
         * @return a random number.
         */
        inline real rnd() {
            return uniform(rng);
        }

        /**
         * @brief Computes the difference quotient between two samples
         * @param a the first sample.
         * @param b the second sample.
         * @return the difference quotient.
         */
        inline real diffQuotient(const Sample& a, const Sample& b) {
            return (b.value - a.value) / (b.u - a.u);
        }

        // The function to be sampled
        IntegrableFunction& f;

        // The current sample
        Sample current;

        // Random number generator
        std::mt19937 rng;

        // Uniform distribution for [0, 1)
        std::uniform_real_distribution<real> uniform;

        // Metropolis samples distribution
        SampleDistribution X;

        // Metropolis statistics
        Stats stats;
};

#endif // METROPOLIS_H
