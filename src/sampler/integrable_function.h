#ifndef INTEGRABLE_FUNCTION_H
#define INTEGRABLE_FUNCTION_H

#include "function.h"

/**
 * @brief Interface for real-valued, integrable, 1-dimensional functions.
 */
class IntegrableFunction : public Function {

    public:

        virtual ~IntegrableFunction() = default;

        /**
         * @brief Integrates the function over the given interval.
         * @param x1 the lower bound of the interval.
         * @param x2 the upper bound of the interval.
         * @return the definite integral.
         */
        virtual real integrate(real x1 = -INF, real x2 = INF) = 0;
};


#endif // INTEGRABLE_FUNCTION_H
