#ifndef EDF_H
#define EDF_H

#include "metropolis.h"

/**
 * @brief Class for defining EDFs based on metropolis samples.
 *
 * Due to optimization regarding the evaluation, this class does not implement the Function interface.
 */
class EmpiricalDensityFunction {

    public:

        /**
         * @brief Constructs a EDF given samples and the current state of the Markov chain from the Metropolis algorithm.
         * @param samples the current distribution of Metropolis samples.
         * @param current the current sample of the Markov chain.
         * @param n the total number of samples.
         */
        EmpiricalDensityFunction(const Metropolis::SampleDistribution& samples, const Metropolis::Sample& current, size_t n)
            : X(samples), current(current), sampleCount(static_cast<real>(n)), itPrev(X.begin()) {
        }

        /**
         * @brief Evaluates the function at coordinate @p u.
         * @param u the u-coordinate at which the function is to be evaluated.
         * @return the value of the function at @p u.
         *
         * The evaluation is optimized by incrementally computing the result based on the result of
         * the previous invocation. Thus, the parameter @p u must be greater or equal to the parameter @p u
         * of the last invocation.
         */
         inline real operator ()(real u) {

            // Iterate over relevant interval
            auto end = X.upper_bound(u);

            for (auto it = itPrev; it != end; it++) {
                sumPrev += it->second;
            }

            itPrev = end;

            // Add current sample if appropriate
            Sum rs = sumPrev;
            if (current.u <= u) {
                rs += current.weight;
            }

            return rs.get() / sampleCount;
        }

    private:

        // The distribution of Metropolis samples
        const Metropolis::SampleDistribution& X;

        // The current sample of the Markov chain
        const Metropolis::Sample& current;

        // The number of samples (including the current sample)
        const real sampleCount;

        // End iterator of previous invocation
        Metropolis::SampleDistribution::const_iterator itPrev;

        // Sum of previous invocation
        Sum sumPrev = 0;
};

#endif // EDF_H
