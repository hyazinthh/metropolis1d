#ifndef SAMPLED_FUNCTION_H
#define SAMPLED_FUNCTION_H

#include "common.h"
#include "integrable_function.h"

#include <map>

/**
 * @brief Class for non-negative functions defined by a range of samples.
 *
 * Functions of this class are defined by a range of samples, which are linearly
 * interpolated to evaluate the function at any coordinate within the domain.
 */
class SampledFunction : public IntegrableFunction
{
    public:

        typedef std::map<real, real, CmpReal> Samples;

        /**
         * @brief Constructs a function from a map representing the samples.
         * @param samples the samples defining the function.
         */
        SampledFunction(const Samples& samples);

        /**
         * @brief Constructs a function from a map representing the samples.
         * @param samples the samples defining the function.
         */
        SampledFunction(Samples&& samples);

        /**
         * @brief Returns the domain of the function.
         * @return the domain of the function.
         */
        virtual Domain getDomain() override;

        /**
         * @brief Integrates the function over the given interval.
         * @param x1 the lower bound of the interval.
         * @param x2 the upper bound of the interval.
         * @return the definite integral.
         */
        virtual real integrate(real x1 = -INF, real x2 = INF) override;

        /**
         * @brief Evaluates the function at coordinate @p x.
         * @param x the x-coordinate at which the function is to be evaluated.
         * @return the value of the function at @p x.
         *
         * The function value is linearly interpolated between the samples.
         */
        virtual real operator()(real x) override {
            return evaluate(x);
        }

    private:

        /**
         * @brief Evaluates the function at coordinate @p x.
         * @param x the x-coordinate at which the function is to be evaluated.
         * @param prev stores iterator pointing to the lesser sample that was used to interpolate the value, can be nullptr.
         * @return the value of the function at @p x.
         *
         * The function value is linearly interpolated between the samples.
         */
        real evaluate(real x, Samples::const_iterator* prev = nullptr);

        /**
         * @brief Computes the area defined by two samples (@p x1, @p y1) and (@p x2, @p y2), where
         * @p x1 <= @p x2.
         *
         * @param x1 the x-coordinate of the first sample.
         * @param x2 the x-coordinate of the second sample.
         * @param y1 the y-coordinate of the first sample.
         * @param y2 the y-coordinate of the second sample.
         * @return the area.
         */
        real computeArea(real x1, real x2, real y1, real y2);

    protected:

        // The samples of the function
        Samples samples;

};

#endif // SAMPLED_FUNCTION_H
