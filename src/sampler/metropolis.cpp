#include "metropolis.h"
#include "cdf.h"
#include "edf.h"

#include <algorithm>
#include <numeric>

Metropolis::Metropolis(IntegrableFunction &func)
    : f(func) {
    std::random_device rd;
    rng.seed(rd());
}

real Metropolis::mutate(real value) {

    // Taken from mitsuba
    real sample = rnd();
    bool add;

    if (sample < static_cast<real>(0.5)) {
        add = true;
        sample *= static_cast<real>(2);
    } else {
        add = false;
        sample = static_cast<real>(2) * (sample - static_cast<real>(0.5));
    }

   constexpr real s1 = static_cast<real>(1) / static_cast<real>(1024);
   constexpr real s2 = static_cast<real>(1) / static_cast<real>(64);
   static const real logRatio = -std::log(s2 / s1);

   real dv = s2 * std::exp(sample * logRatio);

    if (add) {
        value += dv;
        if (value > 1)
            value -= 1;
    } else {
        value -= dv;
        if (value < 0)
            value += 1;
    }

    return value;
}

bool Metropolis::initialize(size_t samplesMin, size_t samplesMax) {

    samplesMax = std::max(samplesMin, samplesMax);
    size_t samples = std::max(static_cast<size_t>(1), samplesMin);

    do {

        // Regularly sample the function
        for (size_t i = 0; i < samples; i++) {

            Sample s;
            s.u = static_cast<real>(i) / static_cast<real>(samples - 1);
            s.value = sample(s.u);
            s.weight = 1;

            if (s.value > current.value || i == 0) {
                current = s;
            }
        }

        // Double the number of samples in case b = 0
        samples <<= 1;

    } while (current.value < EPSILON && samples <= samplesMax);

    // Reset metropolis
    X.clear();
    stats = Stats();
    stats.value.add(current.value);

    // Return if there is contribution
    return current.value >= EPSILON;
}

void Metropolis::run(size_t samples, real pLarge) {

    // Check last invocation of initialize() was successful.
    if (current.value < EPSILON) return;

    // Sample the function
    pLarge = std::max(static_cast<real>(0), std::min(static_cast<real>(1), pLarge));

    for (size_t i = 0; i < samples; i++) {

        bool largeStep = (rnd() < pLarge);

        // Propose new sample
        Sample proposed;
        proposed.u = largeStep ? rnd() : mutate(current.u);
        proposed.value = sample(proposed.u);

        real a = std::min(static_cast<real>(1), proposed.value / current.value);

        if (std::isnan(proposed.value) || proposed.value < 0) {
            std::cout << "Warning: sample with invalid value with u = " << proposed.u << std::endl;
            a = 0;
        }

        // Update stats
        if (largeStep) {
            stats.value.add(proposed.value);
            stats.acceptedLargeSteps.incrementBase();
            stats.nonZeroLargeSteps.incrementBase();

            if (proposed.value >= EPSILON) {
                ++stats.nonZeroLargeSteps;
            }
        } else {
            stats.diffQuotient.add(diffQuotient(current, proposed));
            stats.acceptedSmallSteps.incrementBase();
        }

        // Weight the current and proposed samples based on the acceptance ratio
        current.weight += (1 - a);
        proposed.weight = a;

        // Accept or reject the proposed sample
        if (rnd() < a) {
            X[current.u] += current.weight;
            current = proposed;

            if (largeStep) {
                stats.acceptedLargeSteps++;
            } else {
                stats.acceptedSmallSteps++;
            }
        } else {
            X[proposed.u] += proposed.weight;
        }
    }

    // Update stats
    stats.sampleCount += samples;

    // Sample EDF and CDF to compute K-S error
    const size_t errSampleCount = 4096;
    CumulativeDensityFunction cdf(f);
    EmpiricalDensityFunction edf(X, current, stats.sampleCount + 1);

    stats.ksError = 0;

    for (size_t i = 0; i < errSampleCount; i++) {
        real u = static_cast<real>(i) / static_cast<real>(errSampleCount - 1);
        real x = pss2ds(u);
        stats.ksError = std::max(stats.ksError, std::abs(cdf(x) - edf(u)));
    }
}
