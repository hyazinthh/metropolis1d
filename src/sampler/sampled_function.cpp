#include "sampled_function.h"
#include "sum.h"

#include <limits>
#include <algorithm>

SampledFunction::SampledFunction(const Samples& samples)
    : samples(samples) {

    for (auto s : samples) {
        s.second = std::max(static_cast<real>(0), s.second);
    }
}

SampledFunction::SampledFunction(Samples&& samples)
    : samples(samples) {

    for (auto s : samples) {
        s.second = std::max(static_cast<real>(0), s.second);
    }
}

Function::Domain SampledFunction::getDomain() {

    if (samples.empty()) {
        return Domain(NaN, NaN);
    } else {
        return Domain(samples.begin()->first, samples.rbegin()->first);
    }
}

real SampledFunction::integrate(real x1, real x2) {

    if (samples.empty()) {
        return 0;
    }

    // Clamp interval to domain
    auto d = getDomain();
    x1 = std::max(d.min, std::min(d.max, x1));
    x2 = std::max(d.min, std::min(d.max, x2));

    // Check for special cases x1 = x2 and x1 > x2
    real mul = 1;

    if (x1 > x2) {
        std::swap(x1, x2);
        mul = -1;
    } else if (x2 - x1 < EPSILON) {
        return 0;
    }

    // Evaluate function at x1 and x2, finding corresponding samples
    Samples::const_iterator it1, it2;
    real y1 = evaluate(x1, &it1);
    real y2 = evaluate(x2, &it2);

    // Sum area defined by samples
    Sum area;

    auto curr = it1;
    auto next = std::next(curr);

    while (next != std::next(it2)) {
        area += computeArea(curr->first, next->first, curr->second, next->second);
        ++curr; ++next;
    }

    // Handle corner cases
    area -= computeArea(it1->first, x1, it1->second, y1);
    area += computeArea(it2->first, x2, it2->second, y2);

    return area.get() * mul;
}

real SampledFunction::computeArea(real x1, real x2, real y1, real y2) {

    real d = x2 - x1;

    if (y1 < y2) {
        return d * (y1 + ((y2 - y1) * static_cast<real>(0.5)));
    } else {
        return d * (y1 + ((y2 - y1) * static_cast<real>(0.5)));
    }
}

real SampledFunction::evaluate(real x, Samples::const_iterator* prev) {

    // Find first element that is greater or equal
    auto b = samples.lower_bound(x);

    // Check if beyond domain or equal
    if (b == samples.end()) {
        return NaN;
    } else {
        if (std::abs(x - b->first) < EPSILON) {
            if (prev) { *prev = b; }
            return b->second;
        } else if (b == samples.begin()) {
            return NaN;
        }
    }

    // Previous element must be less
    auto a = std::prev(b);
    if (prev) { *prev = a; }

    // Interpolate
    real x1 = a->first; real x2 = b->first;
    real y1 = a->second; real y2 = b->second;

    real t = (x - x1) / (x2 - x1);

    return (1 - t) * y1 + t * y2;
}
