#ifndef CDF_H
#define CDF_H

#include "integrable_function.h"
#include "sum.h"

#include <algorithm>

/**
 * @brief Class for defining CDFs based on arbritrary integrable functions.
 *
 * Due to optimization regarding the evaluation, this class does not implement the Function interface.
 */
class CumulativeDensityFunction {

    public:

        /**
         * @brief Constructs a CDF based on an arbritrary integrable function.
         * @param func the function to be used.
         */
        CumulativeDensityFunction(IntegrableFunction& func)
            : f(func), d(func.getDomain()), I(func.integrate()), xPrev(d.min) {
        }

        /**
         * @brief Evaluates the function at coordinate @p x.
         * @param x the x-coordinate at which the function is to be evaluated.
         * @return the value of the function at @p x.
         *
         * The evaluation is optimized by incrementally computing the result based on the result of
         * the previous invocation. Thus, the parameter @p x must be greater or equal to the parameter @p x
         * of the last invocation.
         */
        inline real operator ()(real x) {
            x = std::max(static_cast<real>(d.min), std::min(static_cast<real>(d.max), x));

            yPrev += f.integrate(xPrev, x);
            xPrev = x;

            return yPrev.get() / I;
        }

    private:

        // The base function
        IntegrableFunction& f;

        // The domain of the base function
        const Function::Domain d;

        // The integral of the base function
        const real I;

        // x-coordinate of the most recent invocation of f(x)
        real xPrev;

        // Intermediate result of the most recent invocation of f(x), i.e. f(x) * I
        Sum yPrev = 0;
};

#endif // CDF_H
