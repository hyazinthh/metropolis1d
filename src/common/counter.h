#ifndef COUNTER_H
#define COUNTER_H

#include <iostream>
#include "common.h"

/**
 * @brief Utility class to record ratios and percentages.
 */
class Counter
{
    public:

        /**
         * @brief Returns the base.
         * @return the base.
         */
        inline size_t getBase() const {
            return base;
        }

        /**
         * @brief Returns the count.
         * @return the count.
         */
        inline size_t getCount() const {
            return count;
        }

        /**
         * @brief Returns the ratio count / base.
         * @return count / base if base > 0, otherwise 0.
         */
        inline real getRatio() const {
            if (getBase() > 0) {
                return static_cast<real>(getCount()) / static_cast<real>(getBase());
            } else {
                return 0;
            }
        }

        /**
         * @brief Resets both variables to 0
         */
        inline void reset() {
            base = 0;
            count = 0;
        }

        /**
         * @brief Increments the base by the given value.
         * @param value the increment value.
         */
        inline void incrementBase(size_t value = 1) {
            base += value;
        }

        /**
         * @brief Increments the count by the given value.
         * @param value the increment value.
         */
        inline void incrementCount(size_t value = 1) {
            count += value;
        }

        /**
         * @brief Increments the count by 1.
         * @return reference to *this.
         */
        inline Counter& operator ++() {
            count++;
            return *this;
        }

        /**
         * @brief Increments the count by 1.
         * @return a copy of the counter before the increment.
         */
        inline Counter operator ++(int) {
            auto t = Counter(*this);
            ++(*this);
            return t;
        }

    private:

        // Base of the counter
        size_t base = 0;

        // Counter variable
        size_t count = 0;
};

/**
 * @brief Outputs the ratio of the given counter to the given stream.
 * @param out the output stream.
 * @param c the counter.
 * @return the stream.
 */
inline std::ostream& operator<< (std::ostream &out, const Counter& c) {
    out << c.getRatio();
    return out;
}

#endif // COUNTER_H
