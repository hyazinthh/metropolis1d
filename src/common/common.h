#ifndef COMMON_H
#define COMMON_H

#include <cmath>
#include <limits>

typedef double real;

static_assert(std::numeric_limits<real>::is_iec559, "IEEE 754 required");

static constexpr real EPSILON = static_cast<real>(0.0001);

static constexpr real INF = std::numeric_limits<real>::infinity();

static constexpr real NaN = std::numeric_limits<real>::quiet_NaN();

static constexpr real MIN = std::numeric_limits<real>::lowest();

static constexpr real MAX = std::numeric_limits<real>::max();

/**
 * @brief Function object for comparing reals taking the EPSILON constant into
 * consideration.
 */
struct CmpReal {
    inline bool operator()(real a, real b) const {
      return b - a > EPSILON;
    }
};

#endif // COMMON_H
