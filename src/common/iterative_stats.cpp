#include "iterative_stats.h"

#include <algorithm>

void IterativeStats::add(real x) {

    real n = static_cast<real>(++(this->n));
    real n1 = n - 1;

    real d = x - mean;
    real dn = d / n;
    real dn2 = dn * dn;
    real t = d * dn * n1;

    mean += dn;
    M[4] += t * dn2 * (n * n - 3 * n + 3) + 6 * dn2 * M[2] - 4 * dn * M[3];
    M[3] += t * dn * (n - 2) - 3 * dn * M[2];
    M[2] += t;
    M[0] = n;

    min = std::min(min, x);
    max = std::max(max, x);
}
