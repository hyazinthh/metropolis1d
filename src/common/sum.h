#ifndef SUM_H
#define SUM_H

#include "common.h"

/**
 * @brief Utility class to compute sums using Kahan's summation algorithm.
 */
class Sum {

    public:

        Sum() = default;

        Sum(real x) : sum(x) {}

        /**
         * @brief Returns the accumulated sum.
         * @return the sum.
         */
        inline real get() const {
            return sum;
        }

        /**
         * @brief Adds a value to the sum.
         * @param x the value to add.
         */
        inline void add(real x) {
            real y = x - c;
            real t = sum + y;
            c = (t - sum) - y;
            sum = t;
        }

        inline Sum& operator +=(real x) {
            add(x);
            return *this;
        }

        inline Sum& operator +=(const Sum& x) {
            c += x.c;
            add(x.get());
            return *this;
        }

        inline Sum& operator -=(real x) {
            add(-x);
            return *this;
        }

        inline Sum& operator -=(const Sum& x) {
            c -= x.c;
            add(-x.get());
            return *this;
        }

        inline Sum& operator =(real x) {
            sum = x;
            c = 0;
            return (*this);
        }

    private:

        // Accumulated sum
        real sum = 0;

        // Error
        real c = 0;
};

#endif // SUM_H
