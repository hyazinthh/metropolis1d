#include "string.h"

void String::tokenize(const std::string& str, char delim, std::vector<std::string>& tokens) {

    size_t p = 0;
    tokens.clear();

    for (;;) {
        size_t n = str.find_first_of(delim, p);

        if (n == std::string::npos) {
            tokens.push_back(str.substr(p, n));
            return;
        } else {
            tokens.push_back(str.substr(p, n - p));
        }

        p = n + 1;
    }
}

bool String::parse(const std::string& str, real& out) {

    try {
        size_t n = 0;
        real v = static_cast<real>(std::stold(str, &n));

        if (n == 0 || std::isnan(v) || std::isinf(v)) {
            throw std::exception();
        }

        out = v;

    } catch (std::exception&) {
        return false;
    }

    return true;
}

bool String::parse(const std::string& str, size_t& out) {

    try {
        size_t n = 0;
        size_t v = static_cast<size_t>(std::stoul(str, &n));

        if (n == 0) {
            throw std::exception();
        }

        out = v;

    } catch (std::exception&) {
        return false;
    }

    return true;
}

