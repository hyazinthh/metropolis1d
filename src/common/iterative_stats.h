#ifndef ITERATIVE_STATS_H
#define ITERATIVE_STATS_H

#include "common.h"
#include <array>
#include <cmath>

/**
 * @brief Class to iteratively compute statistics from a stream of data.
 */
class IterativeStats {

    public:

        /**
         * @brief Returns the k-th central moment (up to k = 4).
         * @param k the order of the moment.
         * @return the k-th central moment if k < 5, 0 otherwise.
         */
        inline real getMoment(unsigned int k) const {
            if (k < 5) {
                return (n > 0) ? (M[k] / static_cast<real>(n)) : M[k];
            } else {
                return 0;
            }
        }

        /**
         * @brief Returns the sample mean.
         * @return the mean.
         */
        inline real getMean() const {
            return mean;
        }

        /**
         * @brief Returns the sample variance.
         * @return the variance.
         *
         * In contrast to getMoment(), this gives an unbiased estimate by dividing by n - 1 instead of n.
         */
        inline real getVariance() const {
            return (n > 1) ? (M[2] / static_cast<real>(n - 1)) : 0;
        }

        /**
         * @brief Returns the sample skewness.
         * @return the skewness.
         */
        inline real getSkewness() const {
            return (n > 0) ? (M[3] / (static_cast<real>(n) * std::pow(M[2] / static_cast<real>(n), 1.5))) : 0;
        }

        /**
         * @brief Returns the sample kurtosis.
         * @return the kurtosis.
         */
        inline real getKurtosis() const {
            return (n > 0) ? (M[4] / (static_cast<real>(n) * std::pow(M[2] / static_cast<real>(n), 2))) : 0;
        }

        /**
         * @brief Returns the minimum of the sample.
         * @return the minimum.
         */
        inline real getMin() const {
            return min;
        }

        /**
         * @brief Returns the maximum of the sample.
         * @return the maximum.
         */
        inline real getMax() const {
            return max;
        }

        /**
         * @brief Returns the sample size.
         * @return the sample size.
         */
        inline size_t getSize() const {
            return n;
        }

        /**
         * @brief Adds a datum to the sample, updating the statistics.
         * @param x the datum to add.
         */
        void add(real x);

    private:

        // Number of sample data
        size_t n = 0;

        // Sample mean
        real mean = 0;

        // Minimum
        real min = MAX;

        // Maximum
        real max = MIN;

        // Central moments
        std::array<real, 5> M = { 1, 0, 0, 0, 0 };
};

#endif // ITERATIVE_STATS_H
