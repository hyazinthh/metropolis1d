#ifndef STRING_H
#define STRING_H

#include "common.h"
#include <string>
#include <vector>

/**
 * Defines various utility functions for strings.
 */
namespace String {

    /**
     * @brief Splits the given string into tokens.
     * @param str the string to be split.
     * @param delim the character used as delimiter.
     * @param tokens returns the extracted tokens.
     */
    void tokenize(const std::string& str, char delim, std::vector<std::string>& tokens);

    /**
     * @brief Parses the string as real.
     * @param str the string to be parsed.
     * @param out returns the parsed value.
     * @return true on success, false otherwise.
     */
    bool parse(const std::string& str, real& out);

    /**
     * @brief Parses the string as size_t.
     * @param str the string to be parsed.
     * @param out returns the parsed value.
     * @return true on success, false otherwise.
     */
    bool parse(const std::string& str, size_t& out);

}

#endif // STRING_H
